export const Main = 'darkorange';
export const Second = 'white';

export const Item = '#fe9802';
export const Right = 'green';

export const Button = '#804600';
export const ButtonHover = '#552f00';
